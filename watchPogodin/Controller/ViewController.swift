//
//  ViewController.swift
//  wether
//
//  Created by WSR on 20/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    @IBOutlet weak var backgroudImage: UIImageView!
    @IBOutlet weak var butGoToCityTable: UIButton!
    @IBOutlet weak var cityLabel: UILabel!
    var cityLbl: String = ""
    var temp: String = ""
    var bgImg: String = ""
    @IBOutlet weak var tempCity: UILabel!
    var userDefaults = UserDefaults()
    var icon: String = ""
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cityLbl = self.userDefaults.string(forKey: "cityLbl")!
        self.cityLabel.text = self.cityLbl
        
        if self.cityLbl != "" {
            downloadData(city: self.cityLbl)
        }
        
        if let bitmp = UIImage(named: self.userDefaults.string(forKey: "bgImg")!) {
            self.backgroudImage.image = bitmp
            self.backgroudImage.contentMode = .scaleAspectFill
        }
        else {
            self.backgroudImage.image = UIImage(named: "source/weather.png")
            self.backgroudImage.contentMode = .scaleAspectFit
        }
        
        self.butGoToCityTable.layer.cornerRadius = 50
        /*
        if let bitmp = UIImage(named: "source/goToCityTable.png") {
            butGoToCityTable.setTitle("", for: <#T##UIControl.State#>)
            butGoToCityTable.setImage(bitmp, for: <#T##UIControl.State#>)
        }
        else {
            butGoToCityTable.setTitle(">", for: <#T##UIControl.State#>)
        }
        butGoToCityTable.backgroundImage(for: <#T##UIControl.State#>)
 */
        // Do any additional setup after loading the view.
    }
    
    func downloadData(city: String) {
        
        let token = "1e936ee21707e2a418e98dca00877357"
        let urlStr = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&appid=\(token)"
        var url = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        //print("start")
        
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                self.temp = json["main"]["temp"].stringValue
                self.icon = "https://openweathermap.org/img/w/" + json["weather"][0]["icon"].stringValue + ".png"
                self.tempCity.text = self.temp
                //print(json.string)
                self.userDefaults.set(self.icon, forKey: "icon")
                self.userDefaults.set(self.temp, forKey: "temp")
                if let icontmp = UIImage(data: NSData(contentsOf: NSURL(string: self.icon)! as URL)! as Data) {
                    self.weatherIcon.image = icontmp
                    self.weatherIcon.contentMode = .scaleAspectFit
                }
                let lon = json["coord"]["lon"].doubleValue
                let lat = json["coord"]["lat"].doubleValue
                self.userDefaults.set(lon, forKey: "lon")
                self.userDefaults.set(lat, forKey: "lat")
                
                
            case .failure(let error):
                print(error)
            }
        }
        
        let urlFlkr = "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=fe96c1b0698cf5d73c48e7e14623f5f9&tags=" + city + "&format=json&nojsoncallback=?"
        print(urlFlkr)
        url = urlFlkr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let farm = json["photos"]["photo"][0]["farm"].stringValue
                let server = json["photos"]["photo"][0]["server"].stringValue
                let id = json["photos"]["photo"][0]["id"].stringValue
                let secret = json["photos"]["photo"][0]["secret"].stringValue
                let urlImg = "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg"
                print(urlImg)
                if let bgtmp = UIImage(data: NSData(contentsOf: NSURL(string: urlImg)! as URL)! as Data) {
                    self.backgroudImage.image = bgtmp
                    self.backgroudImage.contentMode = .scaleAspectFill
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    
}

