//
//  CityTableViewController.swift
//  wether
//
//  Created by WSR on 20/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class CityTableViewController: UITableViewController {

    var arr: [String] = []
    let realm = try! Realm()
    var cityArr: Results<City>!
    
    var userDefaults = UserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //arr = userDefaults.stringArray(forKey: "arrCity") ?? [String]()
        cityArr = realm.objects(City.self)
        tableView.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //return arr.count
        return cityArr.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as! CityTableViewCell
        //cell.cityLabel?.text = arr[indexPath.row]
        cell.cityLabel?.text = cityArr[indexPath.row].name
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            self.realm.delete(self.cityArr[indexPath.row])
            //cityArr.removeObserver(<#T##observer: NSObject##NSObject#>, forKeyPath: <#T##String#>)
            //arr.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCityMain" {
            //let vc = segue.destination as! ViewController
            
            let index = self.tableView.indexPathForSelectedRow
            
            self.userDefaults.set(cityArr[(index?.row ?? 0)].name, forKey: "cityLbl")
            //self.userDefaults.set(arr, forKey: "arrCity")
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

    @IBAction func addCity(_ sender: Any) {
        print("er")
        let alert = UIAlertController(title: "Добавить город", message: "Введите город", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            
        }
        
        alert.addAction(UIAlertAction(title: "Добавить", style: UIAlertAction.Style.default, handler: {
            (alertAction) in
            
            let textField = alert.textFields![0] as UITextField
            
            self.downloadSetData(city: textField.text!)
        }))
        
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: { (alertAction) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func downloadSetData(city: String) {
        let token = "1e936ee21707e2a418e98dca00877357"
        let urlStr = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&appid=\(token)"
        let url = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        //print("start")
        
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let lon = json["coord"]["lon"].doubleValue
                let lat = json["coord"]["lat"].doubleValue
                let temp = json["main"]["temp"].doubleValue
                let icon = "https://openweathermap.org/img/w/" + json["weather"][0]["icon"].stringValue + ".png"
                //self.userDefaults.set(lon, forKey: "lon")
                //self.userDefaults.set(lat, forKey: "lat")
                let newCity = City(value: ["name": city, "lat": lat, "lon": lon, "lastTemp": temp, "lastIcon": icon])
                try! self.realm.write {
                    self.realm.add(newCity)
                }
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
