//
//  AnnotationView.swift
//  watchPogodin
//
//  Created by WSR on 23/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import UIKit

class AnnotationView: UIView {

    @IBOutlet weak var iconWeather: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityTemp: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
