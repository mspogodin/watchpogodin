//
//  MapViewController.swift
//  watchPogodin
//
//  Created by WSR on 23/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import RealmSwift
import SwiftyJSON
import Alamofire

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    let realm = try! Realm()
    var cityArr: Results<City>!
    var userDefaults = UserDefaults()
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    //var geoTarget = "city"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            self.locationManager.delegate = self
        }
        else {
            self.locationManager.requestAlwaysAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.startUpdatingLocation()
        }
        
        
        self.cityArr = realm.objects(City.self)
        for i in 0..<self.cityArr.count {
            self.showCityOnMap(index: i)
        }
        self.showCityOnMap(lon: self.userDefaults.double(forKey: "lon"), lat: self.userDefaults.double(forKey: "lat"))
        // Do any additional setup after loading the view.
    }
    
    func showCityOnMap(str: String, lon: Double, lat: Double) {
        let regionRadius: CLLocationDistance = 10000
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let coordinateRegion = MKCoordinateRegion(center: coord, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: false)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coord
        
        if geoTarget == "I" {
            annotation.title = str
        }
        self.mapView.addAnnotation(annotation)
        
    }
    
    func showCityOnMap(city: City) {
        let regionRadius: CLLocationDistance = 10000
        let coord = CLLocationCoordinate2D(latitude: city.lat, longitude: city.lon)
        let coordinateRegion = MKCoordinateRegion(center: coord, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: false)
        
        let annotView = MKAnnotationView()
        let annotPoint = MKPointAnnotation()
        annotPoint.coordinate = coord
        annotPoint.title = city.name
        annotView.annotation = annotPoint
        self.mapView.addAnnotation(annotView.annotation!)
        
    }
    
    func showCityOnMap(index: Int) {
        let regionRadius: CLLocationDistance = 10000
        let coord = CLLocationCoordinate2D(latitude: cityArr[index].lat, longitude: cityArr[index].lon)
        let coordinateRegion = MKCoordinateRegion(center: coord, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: false)
        
        let annotView = MKAnnotationView()
        let annotPoint = MKPointAnnotation()
        annotPoint.coordinate = coord
        annotPoint.title = String(index)
        annotView.annotation = annotPoint
        self.mapView.addAnnotation(annotView.annotation!)
        
    }
    
    func showCityOnMap(lon: Double, lat: Double) {
        let regionRadius: CLLocationDistance = 10000
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let coordinateRegion = MKCoordinateRegion(center: coord, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: true)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        } else {
            if self.geoTarget == "city" {
                let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
            
                let xib = Bundle.main.loadNibNamed("AnnotationView", owner: self, options: nil)?.first as! AnnotationView
                if let i = Int(((annotationView.annotation?.title)!)!) {
    //                let city = realm.objects(City.self).filter("name == " + (annotationView.annotation?.title!)!)
                    xib.cityLabel.text = self.cityArr![i].name
                    xib.cityTemp.text = self.cityArr![i].getLastTempStr()
                    if let icontmp = UIImage(data: NSData(contentsOf: NSURL(string: self.cityArr![i].lastIcon)! as URL)! as Data) {
                        xib.iconWeather.image = icontmp
                        xib.iconWeather.contentMode = .scaleAspectFill
                    }
                }
                annotationView.addSubview(xib)
                annotationView.centerOffset.x = annotationView.centerOffset.x - 50
                annotationView.centerOffset.y = annotationView.centerOffset.y - 50
                annotationView.canShowCallout = false
                annotationView.isDraggable = true
                return annotationView
            } else {
                return MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
            }
        }
    }
    
    var geoTarget: String = "city" // "I"
    
    @IBAction func editGeoTarget(_ sender: Any) {
        switch geoTarget {
        case "city":
            print(geoTarget)
            self.geoTarget = "I"
            self.mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.stopUpdatingLocation()
                self.showCityOnMap(lon: self.locationManager.location?.coordinate.longitude ?? 0, lat: self.locationManager.location?.coordinate.latitude ?? 0)
                self.locationManager.startUpdatingLocation()
            }
        case "I":
            print(geoTarget)
            self.mapView.showsUserLocation = false
            self.geoTarget = "city"
            //self.showCityOnMap(str: self.userDefaults.string(forKey: "cityLbl")!, lon: self.userDefaults.double(forKey: "lon"), lat: self.userDefaults.double(forKey: "lat"))
        default:
            print("error geoTarget")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations
        locations: [CLLocation]) {
        print("LM")
//        if geoTarget == "I" {
//            locationManager.stopUpdatingLocation()
//            self.showCityOnMap(str: "I'm here", lon: self.locationManager.location!.coordinate.longitude, lat: self.locationManager.location!.coordinate.latitude)
////            let radius = CLLocationDistance(10000)
////            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations.last!.coordinate.latitude, longitude: locations.last!.coordinate.longitude), latitudinalMeters: radius, longitudinalMeters: radius)
//            //self.mapView.setRegion(region, animated: true)
//        }
//        else {
//            self.showCityOnMap(str: self.userDefaults.string(forKey: "cityLbl")!, lon: self.userDefaults.double(forKey: "lon"), lat: self.userDefaults.double(forKey: "lat"))
            
//        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
