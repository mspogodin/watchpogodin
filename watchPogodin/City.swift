//
//  City.swift
//  watchPogodin
//
//  Created by WSR on 24/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import UIKit
import RealmSwift

class City: Object {
    @objc dynamic var name = ""
    @objc dynamic var lat: Double = 0.0
    @objc dynamic var lon: Double = 0.0
    @objc dynamic var lastTemp: Double = 0.0
    @objc dynamic var lastIcon: String = ""
    
    func getLatStr() -> String {
        return String(lat)
    }
    
    func getLonStr() -> String {
        return String(lon)
    }
    
    func getLastTempStr() -> String {
        return String(lastTemp)
    }
    
    func getCityName() -> String {
        return name
    }
    
    func setCityData(name: String?, lat: Double, lon: Double) -> Bool{
        if let nm = name {
            self.name = nm
            self.lat = lat
            self.lon = lon
            return true
        } else {
            print("error setCityData")
            return false
        }
    }
}
