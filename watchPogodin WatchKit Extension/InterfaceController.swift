//
//  InterfaceController.swift
//  watchPogodin WatchKit Extension
//
//  Created by WSR on 22/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import WatchKit
import Foundation
import MapKit
import CoreLocation
import Alamofire
import SwiftyJSON


class InterfaceController: WKInterfaceController {

    var userDefaults = UserDefaults()
    
    @IBOutlet weak var mapView: WKInterfaceMap!
    @IBOutlet weak var btn: WKInterfaceButton!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        let city = "moscow"
        self.userDefaults.set(city,forKey: "cityLabel")
        self.downloadData(city)
        // Configure interface objects here.
    }
    
    func downloadData(_ city: String) {
        print(city)
        let urlOpWM = "https://api.openweathermap.org/data/2.5/weather?q=moscow&units=metric&appid=1e936ee21707e2a418e98dca00877357"
        let url = urlOpWM.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            print("start")
            switch response.result {
            case .success(let value):
                let json = JSON(value)
            self.userDefaults.set(json["main"]["temp"].stringValue, forKey: "cityTemp")
            self.userDefaults.set("https://openweathermap.org/img/w/" + json["weather"][0]["icon"].stringValue + ".png", forKey: "urlIcon")
                let lon = json["coord"]["lon"].doubleValue
                let lat = json["coord"]["lat"].doubleValue
                self.userDefaults.set(lon, forKey: "lon")
                self.userDefaults.set(lat, forKey: "lat")
                self.showCityOnMap(city: city, lon: lon, lat: lat)
                /*
                if let icon = UIImage(data: NSData(contentsOf: NSURL(string: urlIcon)! as URL)! as Data) {
                    self.iconWeather.image = icon
                    self.iconWeather.contentMode = .scaleAspectFit
                }
                 */
            case .failure(let error):
                print(error)
            }
        }
        
//        let urlFlkr = "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=fe96c1b0698cf5d73c48e7e14623f5f9&tags=" + city + "&format=json&nojsoncallback=?"
//        url = urlFlkr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        Alamofire.request(url!, method: .get).validate().responseJSON { response in
//            switch response.result {
//            case .success(let value):
//                let json = JSON(value)
//
//                let farm = json["photos"]["photo"][0]["farm"].stringValue
//                let server = json["photos"]["photo"][0]["server"].stringValue
//                let id = json["photos"]["photo"][0]["id"].stringValue
//                let secret = json["photos"]["photo"][0]["secret"].stringValue
//                self.userDefaults.set("https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg", forKey: "urlBg")
//                let lon = json["coord"]["lon"].doubleValue
//                let lat = json["coord"]["lat"].doubleValue
//                self.userDefaults.set(lon, forKey: "lon")
//                self.userDefaults.set(lat, forKey: "lat")
//                self.showCityOnMap(city: city, lon: lon, lat: lat)
//                /*
//                if let bg = UIImage(data: NSData(contentsOf: NSURL(string: urlBg)! as URL)! as Data) {
//                    self.backgroundImage.image = bg
//                    self.backgroundImage.contentMode = .scaleAspectFill
//                }
//                */
//            case .failure(let error):
//                print(error)
//            }
//        }
    }

    func showCityOnMap(city: String, lon: Double, lat: Double) {
        print("map")
        let rR: CLLocationDistance = 1000
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let cR = MKCoordinateRegion(center: coord, latitudinalMeters: rR, longitudinalMeters: rR)
        mapView.setRegion(cR)
        print("map")
//        let point = MKMapPoint(coord)
//        var annotation = MKStringFromMapPoint(point)
//        annotation.append(contentsOf: city + self.userDefaults.string(forKey: "cityTemp")!)
        mapView.addAnnotation(coord, with: WKInterfaceMapPinColor.green)
//        print("map")
        //mapView.addAnnotation(coord, withImageNamed: "img_571003.png", centerOffset: CGPoint(x: 0, y: 0))
        print("map")
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func goToData(_ sender: Any) {
        
        pushController(withName: "cityData", context: nil    )
    }
 
}
