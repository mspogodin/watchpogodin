//
//  CityInterfaceController.swift
//  watchPogodin WatchKit Extension
//
//  Created by WSR on 22/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import WatchKit
import Foundation


class CityInterfaceController: WKInterfaceController {

    @IBOutlet weak var cityLabel: WKInterfaceLabel!
    @IBOutlet weak var cityTemp: WKInterfaceLabel!
    @IBOutlet weak var cityWeatherIcon: WKInterfaceImage!
    
    
    var userDefaults = UserDefaults()
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.cityLabel.setText(self.userDefaults.string(forKey: "cityLabel")!)
        self.cityTemp.setText(self.userDefaults.string(forKey: "cityTemp")!)
        
        if let icon = UIImage(data: NSData(contentsOf: NSURL(string: self.userDefaults.string(forKey: "urlIcon")!)! as URL)! as Data) {
            self.cityWeatherIcon.setImage(icon)
        }
        print(self.userDefaults.string(forKey: "cityTemp")!)
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
